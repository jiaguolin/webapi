//
//  ViewController.swift
//  WebApi
//
//  Created by jiaguolin on 2017/4/25.
//  Copyright © 2017年 emicnet. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        requestUrl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
     企业名称：福建API测试账户
     主账户名：福建API郭志旭测试主账号
     调用地址：apitest.emic.com.cn
     版本号：20161206
     总机号：059138255501
     accountSid: b4ffd69fbe3e1b4292ca6484394f78fd
     authToken:  78289bfdcdfb5f0403967c43e8fe5ea5
     subAccountSid: 416efba2937a2bae798bad189c7cff2d
     subAccountToken: 71cce8dbab2482864963863a25767436
     appId: 453bf0936ce11fe8ad5909ea40e8ef2f
    */
    func requestUrl(){
 
        let sid = "b4ffd69fbe3e1b4292ca6484394f78fd"
        
        let token = "78289bfdcdfb5f0403967c43e8fe5ea5"
        
        
        //获取当前时间
        let now = Date()
        
        
        // 创建一个日期格式器
        let dformatter = DateFormatter()
        dformatter.dateFormat = "yyyyMMddHHmmss"
        print("当前日期时间：\(dformatter.string(from: now))")
        
        let dateStr = dformatter.string(from: now)
            
        let sig = sid + token + dateStr
        
        let urlString = "http://apitest.emic.com.cn/20161206/Accounts/" + sid + "/AccountInfo?sig=" + sig.md5().uppercased()
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.setValue("application/xml", forHTTPHeaderField: "Accept")
        
        request.setValue("application/xml;charset=utf-8", forHTTPHeaderField: "Content-Type")

        let authorization = sid + ":" + dateStr
        
        let plainData = authorization.data(using: String.Encoding.utf8)
        let base64String  = plainData!.base64EncodedString()
        
        //<账号Id:时间戳>，用base64编码；账号Id与sigParameter中相同；
        request.addValue(base64String, forHTTPHeaderField: "Authorization")
        let urlSession = URLSession.shared
        
//        let httpBodyStr = "appId=453bf0936ce11fe8ad5909ea40e8ef2f&From=18550259787&To=15298371625"
//        request.httpBody = httpBodyStr
        
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            
            if error != nil{
            
                print(error.debugDescription)
            }else{
            
                let result = String(data: data!, encoding: .utf8)
                
                print(result!)
            }

        }
        
        dataTask.resume()
    }

}

extension String{
    func md5() ->String!{
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        CC_MD5(str!, strLen, result)
        let hash = NSMutableString()
        for i in 0 ..< digestLen {
            hash.appendFormat("%02x", result[i])
        }
        result.deinitialize()
        return String(format: hash as String)
    }
}


